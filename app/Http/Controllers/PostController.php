<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    }
    public function create(){
        return view("posts.create",[]);
    }
    public function store(PostRequest $request){
        $data=array(
            'title'=>$request->title,
            "description"=>$request->description
        );
        Post::create($data);
        return Redirect::route('post.create');
    }
    public function show($id){

    }
    public function edit($id){

    }
    public function update($id){

    }
    public function destroy($id){

    }

}
